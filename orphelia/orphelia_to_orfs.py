#!/usr/bin/env python
#
#Copyright 2013 Peter van Heusden <pvh@sanbi.ac.za>
#
#This file is part of Peter van Heusden's shared bioinformatics scripts collection.
#
#Peter van Heusden's shared bioinformatics scripts collection is free software: 
#you can redistribute it and/or modify it under the terms of the GNU General 
#Public License as published by the Free Software Foundation, either version 3 
#of the License, or (at your option) any later version.
#
#Peter van Heusden's shared bioinformatics scripts collection is distributed 
#in the hope that it will be useful, but WITHOUT ANY WARRANTY; without 
#even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE.  See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Peter van Heusden's shared bioinformatics scripts collection.  
#If not, see <http://www.gnu.org/licenses/>.
#
# input:
# 1) FASTA file of nucleotides
# 2) Orphelia output
# 
# Orphelia output format:
#>6,1_154_463_+_1_I_wood_S1_L001_R2_001_q30_contig_6 Average coverage: 2.75 
#
#gene.pred : The final prediction in coord format
#>FragmentNo,ORFinFragNo_posleft_posRight_+|-_Frame_C|I_FragHeader
#FragmentNo : Number of the fragment (resp. input order)
#ORFinFragNo : Counter of the predicted ORF in this fragment (simply incremented)
#posLeft : Left coordinate of the ORF in the fragment sequence
#posRight : Right coordinate of the ORF in the fragment sequence
#+|- : Strand
#Frame : Reading frame of a predicted gene, counted from the 5'-end of the input sequence. Reading frame 1 begins at the 1st position of the sequence, reading frame 2 at the 2nd, frame 3 at the third position.
#Examples for Frame:
 #----------------------  DNA Fragment
    #ATG------TAG         Gene, begins on position 4 -> Fr. 1

#----------------------  DNA Fragment
   #ATG------TAG          Gene, begins on pos. 3 -> Fr. 3

#----------------------  DNA Fragment
     #ATG------TAG       Gene, begins on pos. 5 -> Fr. 2
#C|I : Is the predicted gene complete (C) of incomplete (I)
#
#coordinates are 1 based

import re
import argparse
import sys
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC

parser = argparse.ArgumentParser(description='Given Orphelia output and FASTA file of genomic sequence, output collection of ORFs')
parser.add_argument('genome_fasta_filename', type=str, help='Genome file in FASTA format')
parser.add_argument('orphelia_file', type=argparse.FileType(), help='Orphelia gene.pred output file')
parser.add_argument('output_fasta', type=argparse.FileType('w'), help='Output filename for ORFs in FASTA format')
parser.add_argument('--verbose', '-v', action='store_true', default=False, help='Print verbose output to stderr')
parser.add_argument('--gff_output', type=argparse.FileType('w'), help='Write GFF3 argument describing ORF features on genome')
args = parser.parse_args()

genome_idx = SeqIO.index(args.genome_fasta_filename, 'fasta')
orphelia_re = re.compile('>(\d+),(\d+)_(\d+)_(\d+)_([+-])_([123])_([IC])_(\S+) (Average coverage: (\S+))?')
if args.gff_output:
	args.gff_output.write('##gff-version 3\n')	
for line in args.orphelia_file:
	orphelia_match = orphelia_re.match(line)
	if orphelia_match:
		(genfrag_num, orf_num, start, end, strand, frame, completeness_flag, genfrag_id) = orphelia_match.group(1,2,3,4,5,6,7,8)
		if orphelia_match.lastindex == 10:
			coverage = orphelia_match.group(10)
		else:
			coverage = None
		genfrag_seqrecord = genome_idx[genfrag_id]
		start = int(start)
		end = int(end)
		orf_id = '_'.join([genfrag_id, orf_num])
		if args.verbose:
			sys.stderr.write("fragment {} size: {}, match_id: {} match size: {} codons: {:.2f} complete: {}\n".format(genfrag_id, len(genfrag_seqrecord.seq),
			                                                                                                      orf_id, end-start, float(end-start+1)/3, completeness_flag ))
		orf_seqrecord = SeqRecord(Seq(str(genfrag_seqrecord.seq[start-1:end]), alphabet=IUPAC.ambiguous_dna), id=orf_id, description=genfrag_seqrecord.description)
		SeqIO.write(orf_seqrecord, args.output_fasta, 'fasta')
		if args.gff_output:
			attributes = 'ID={0}; completeness={1}'.format(orf_id, completeness_flag)
			gff_line = '\t'.join([genfrag_id, 'orphelia', 'ORF', str(start), str(end), '.', strand, '.', attributes]) + '\n'
			args.gff_output.write(gff_line)
